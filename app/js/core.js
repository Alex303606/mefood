$(document).ready(function() {    
    function e() {
        $(".slick-slide").removeClass("active-slide");
        $(".slick-slide p").hide();
        $(".slick-current").next(".slick-active").addClass("active-slide"); 
        $(".slick-active:last-child").removeClass("active-slide");
        setTimeout(function() {
            $('.active-slide p').show();                        
        }, 300);
    }
    var i = document.createElement("meta");
    i.setAttribute("name", "viewport"), screen.width < 480 ? i.setAttribute("content", "width=480") : i.setAttribute("content", "width=device-width, initial-scale=1"), document.head.appendChild(i),
    $(".selectric").selectric(), 
    $(".btn-toggle").on("click", function() {
        $("#dropdown").slideToggle()
    }),      
    $(window).width() < 760 && ($("#label-weight1").text("Вес, кг"), $("#label-height1").text("Рост, см")), 
     
    $(".food1").is(":checked") && ($(".food1").next("label").css("border-right", "2px solid #8bc058"), $(".food2").next("label").css("border-left", "none")), $(".food2").is(":checked") && ($(".food2").next("label").css("border-left", "2px solid #8bc058"), $(".food1").next("label").css("border-right", "none")), 
          
    $(document).on("click", ".show-call-back", function() {
        $(".modal.call-back").show(), $("body").append('<div id="modal-wrap"></div>')
    }), 
    $(document).on("click", ".modal-programm.light", function() {
        $(".modal.programm.light").show(), $("body").append('<div id="modal-wrap"></div>')
    }), 
    $(document).on("click", ".modal-programm.sport", function() {
        $(".modal.programm.sport").show(), 
        $("body").append('<div id="modal-wrap"></div>')
    }),
    $(document).on("click", ".modal-programm.indiv", function() {
        $(".modal.programm.indiv").show(), 
        $("body").append('<div id="modal-wrap"></div>')
    }),
    $(document).on("click", "a.close", function() {
        $(".modal").hide(), $("#modal-wrap").remove()
    }), 
    $(document).on("click", "#modal-wrap", function() {
        $(".modal").hide(), $(this).remove()
    }), 
    $(".question-list li").on("click", function() {
        $(".question-list li").removeClass("radius"),
         $(this).toggleClass("open"),
         $(this).hasClass("open") ? ($(this).find('a.open-close').find("img").remove(), $(this).find('a.open-close').append('<img src="img/close.png" alt="">'), $(this).find(".dropdown").slideDown()) : ($(this).find('a.open-close').find("img").remove(), $(this).find('a.open-close').append('<img src="img/open.png" alt="">'), $(this).find(".dropdown").slideUp())
    }), 
    $(".slider-small").slick({
        autoplay: true
    }), $(".slider").slick({
        dots: !1,        
        infinite: true,
        speed: 200,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1171,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: !0,
                dots: !1
            }
        }]
    }), 
    $(document).on("swipe", ".slider", function(i, t, s, o) {
        e()
    }), 
    $(document).on("afterChange", ".slider", function(i, t, s, o) {
        e()
    })
    $('.select-items').find('.item').addClass('item-select');
    $('input.food1+label').on('click',function(){
        $(this).closest('.top-form').next('.select-items').find('.item-select').removeClass('active').css('pointer-events','auto');        
    	$(this).closest('.select-row').find('.result-span').text('3 приема на выбор');
        var selected = $(this).closest('.top-form').next('.select-items').find('.item-select').find('.btn-select');
        $(selected).html("Выбрать");
    });    

    $('input.food2+label').on('click',function(){
        $(this).closest('.top-form').next('.select-items').find('.item-select').addClass('active').css('pointer-events','none');        
        var selected = $(this).closest('.top-form').next('.select-items').find('.item-select').find('.btn-select');
    	$(this).closest('.select-row').find('.result-span').text('');        
        $(selected).html("Отменить выбор");
    });

    $('.click-active').on('click',function(){
        $(this).closest('ul').find('.click-active').removeClass('active');
        $(this).toggleClass('active');
    });
    
    $(document).on('click','.item-select', function(){
        $(this).toggleClass('active');        
        "Отменить выбор" == $(this).find('.btn-select').html() ? $(this).find('.btn-select').html("Выбрать") : $(this).find('.btn-select').html("Отменить выбор");
        $('.noselect').removeClass('active');
        $('.noselect').find('.btn-select').html("Выбрать");
    });

    if($('.food1').is(':checked')) { 
        $('.item-select').removeClass('active').css('pointer-events','auto');        
    }

    if($(window).width() >= 1171) {
        $('.active-slide p').show();
        $(document).on('click','.slick-active .image',function(){
            var number = $(this).closest('.slick-active').attr('data-slick-index');
            console.log(number);
            $('.slider').slick('slickGoTo',number-1);        
        });        
    }

    $("input[name='phone']").attr('type','tel');    

    if($(window).width() >= 1170) {
        $("input[name='phone']").mask("+7 (999) 999-99-99");        
    }
    else {           
        $("input[name='phone']").inputMask('+7 (999) 999-99-99',{});         
    }   

    if($(window).width() >= 1170) {
        $("input[name='name']").bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^a-zA-Zа-яА-ЯЁё]/g,'') );
        });        
    }
    else {
        $("input[name='name']").inputMask('AAAAAAAAAAAAAAAAAAAAAAAAAAAAA',{
            A: /[A-Za-zА-Яа-яЁё ]/
        });        
    }

    //Проверка, если IOS
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    if(iOS) {        
        $('.modal').addClass('ios');
        $('.show-call-back').on('click',function(){              
            $('body,html').animate({scrollTop: 0}, 400);           
        });
        $('.modal-programm.light').on('click',function(){              
            var top = $(this).offset().top - 500;            
            $('.modal.programm.light').css('top',top);
            $('body,html').animate({scrollTop: top - 100}, 400);
        });
        $('.modal-programm.sport').on('click',function(){              
            var top = $(this).offset().top - 500;            
            $('.modal.programm.sport').css('top',top);
            $('body,html').animate({scrollTop: top - 100}, 400);
        });
        $('.modal-programm.indiv').on('click',function(){              
            var top = $(this).offset().top - 500;            
            $('.modal.programm.indiv').css('top',top);
            $('body,html').animate({scrollTop: top - 100}, 400);
        });
    }


});