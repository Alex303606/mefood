var gulp = require('gulp'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	rigger = require('gulp-rigger');
	
gulp.task('sass',function() {
	gulp.src('app/sass/style.sass') //Взять файлы
	.pipe(sass({outputStyle:
		'compressed'}).on('error',sass.logError)) //Компилируем Sass
	.pipe(autoprefixer())
	.pipe(gulp.dest('app/css/'));
});

gulp.task('html',function(){
	gulp.src('app/build/*.html')
	.pipe(rigger())
	.pipe(gulp.dest('app/'))
});

gulp.task('compress', function() {
	gulp.src('app/js/*.js' , '!app/js/*.min.js')
    .pipe(rename({suffix: ".min"}))
    .pipe(uglify())
    .pipe(gulp.dest('app/js/'));
});

gulp.task('img_min', function() {
  gulp.src('app/img/*.jpeg')
  .pipe(imagemin())
  .pipe(gulp.dest('app/img'))
});

gulp.task('watch',function(){
	gulp.watch('app/sass/**/*.sass',
		function(event, cb) {
			setTimeout(function(){
			gulp.start('sass');
			}, 1000);
		});
	gulp.watch('app/part/**/*.html', ['html']);
	gulp.watch('app/build/*.html',['html']);
	gulp.watch('app/js/*.js',['compress']);
});

gulp.task('default',['watch','sass','html']); //Задание по умолчанию


